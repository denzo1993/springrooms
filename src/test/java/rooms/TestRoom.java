/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rooms;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Денис
 */
public class TestRoom {
    
    private Room room;
    
    public TestRoom() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        room = new Room();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void increaseTemp() {
        double prevTemp = room.getTemp();
        double increase = 1.0;
        room.increaseTemp(increase);
        double currTemp = room.getTemp();
        assertTrue((currTemp - increase) == prevTemp);
    }
    
    @Test
    public void reduceTemp() {
        double prevTemp = room.getTemp();
        double reduce = 1.0;
        room.increaseTemp(reduce);
        double currTemp = room.getTemp();
        assertTrue((currTemp + reduce) == prevTemp);
    }
}
