/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elements;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import rooms.Room;
import rooms.elements.Controller;

/**
 *
 * @author Денис
 */
public class TestController extends TestCase {
    
    private Room room;
    
    public TestController() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    @Override
    public void setUp() {
        room = new Room();
    }
    
    @After
    @Override
    public void tearDown() {
    }

    @Test
    public void testCheckTemp() {
       room.setMin_temp(10.0);
       room.setMax_temp(20.0);
       room.setTemp(15.0);
       assertTrue(room.getController().checkTemp());
    }
    
    @Test
    public void testCheckLighting() {
        room.setMin_lighting(0.0);
        room.setNorm_lighting(0.5);
        room.setNorm_lighting(1.0);
        assertTrue(room.getController().checkLighting());
    }
}
