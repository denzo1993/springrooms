/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elements;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import rooms.Room;

/**
 *
 * @author Р”РµРЅРёСЃ
 */
public class TestWindow extends TestCase {
    
    private Room room;
    
    public TestWindow() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    @Override
    public void setUp() {
        room = new Room();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testOpen() {
        room.getWindow().open();
        assertTrue(room.getWindow().isOpened());
    }
    
    @Test
    public void testClose() {
        room.getWindow().close();
        assertFalse(room.getWindow().isClosed());
    }
}
