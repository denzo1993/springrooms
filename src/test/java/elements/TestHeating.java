/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elements;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import rooms.Room;

/**
 *
 * @author Денис
 */
public class TestHeating extends TestCase {
    
    private Room room;
    
    public TestHeating() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        room = new Room();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSwitch_on() {
        room.getHeating().switch_on();
        assertTrue(room.getHeating().isActive());
    }
    
    @Test
    public void testSwitch_off() {
        room.getHeating().switch_off();
        assertFalse(room.getHeating().isActive());
    }
}
