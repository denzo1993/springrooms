/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package i_tests;

import javax.annotation.Resource;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import rooms.Room;
import rooms.elements.Lamp;

/**
 *
 * @author Денис
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:TestBeans.xml"})
public class TestLampRoom {
    
    @Resource(name = "room1")
    Room room;
    
    @Resource(name = "lamp1")
    Lamp lamp;
    
    @Test
    public void testLamp() {
        lamp.setLAMP_ON(true);
        Assert.assertEquals(room.getLamp(), lamp);
        Assert.assertTrue(room.getLamp().isLAMP_ON());
    }
}
