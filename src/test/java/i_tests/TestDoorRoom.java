/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package i_tests;

import javax.annotation.Resource;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import rooms.Room;
import rooms.elements.Door;

/**
 *
 * @author Денис
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:TestBeans.xml"})
public class TestDoorRoom {
    
    @Resource(name = "room1")
    Room room;
    
    @Resource(name = "door1")
    Door door;
    
    @Test
    public void testDoor() {
        door.close();
        Assert.assertEquals(door, room.getDoor());
        Assert.assertTrue(room.getDoor().isClosed());
    }
}
